Gem::Specification.new do |gem|
  gem.name              = 'chef-handler-servicenow'
  gem.summary           = 'A Chef handler to interact with ServiceNow.'
  gem.description       = 'This Chef handler will report the events and results for a chef-client run to a ServiceNow instance.'
  gem.license           = 'Apache-2.0'
  gem.version           = '0.3.1'
  gem.date              = '2019-07-22'
  gem.authors           = ['Dan Gordon']
  gem.email             = ['chef@dangordon.xyz']
  gem.homepage          = 'https://gitlab.com/dg42/chef-handler-servicenow'
  gem.files             = ['lib/servicenow.rb', 'lib/servicenow/client.rb']
  gem.extra_rdoc_files  = ['README.md', 'LICENSE']

  gem.add_development_dependency 'httparty', ' ~> 0.17.0'
  gem.add_development_dependency 'chef', ' ~> 14.0'
end
