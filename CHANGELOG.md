# chef-handler-servicenow CHANGELOG

This file is used to list changes made in each version of the chef-handler-servicenow gem.

## 0.3.1 (2019-07-23)
- Add log statements on incident creation.

## 0.3.0 (2019-07-23)
- Add ability to specify `incident_payload` in config hash.
- README updates.

## 0.2.0 (2019-07-22)
- Override run report functions.
- Update to reboot logic.
- Restructure some things.
- Update README.

## 0.1.0 (2019-07-20)
- Initial release.