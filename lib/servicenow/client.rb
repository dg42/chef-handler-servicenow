require 'json'

begin
  require 'httparty'
rescue LoadError
  Chef::Log.warn('chef-handler-servicenow requires the `httparty` gem.')
end

class ServiceNow::Client
  include HTTParty

  def initialize(config = {})
    @config = config
    @instance = @config['instance']
    @username = @config['username']
    @password = @config['password']

    # Set httparty default_options
    self.class.base_uri "https://#{@instance}.service-now.com/"
    self.class.basic_auth @username, @password
    self.class.headers 'Content-Type' => 'application/json'
  end

  def get(url, params)
    self.class.get(url, query: params)
  end

  def post(url, body)
    self.class.post(url, body: body.to_json)
  end
end
