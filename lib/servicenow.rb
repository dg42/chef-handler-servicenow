require 'chef'
require 'chef/handler'

class ServiceNow < Chef::Handler
  def initialize(config = {})
    @client = Client.new(config)
    @incident_payload = config['incident_payload']
    @table_api = '/api/now/table'
  end

  def create_incident(body)
    response = @client.post("#{@table_api}/incident", body)
    if response.code == 201
      Chef::Log.warn("Created incident #{response.parsed_response['result']['number']}")
    else
      Chef::Log.error("Error creating incident: web service returned code #{response.code}: #{response.parsed_response['error']['message']}")
    end
  end

  def run_report_safely(run_status)
    # only allow unsafe if reboot has been requested, otherwise follow default logic
    if run_status.run_context.reboot_requested?
      run_report_unsafe(run_status)
    else
      _run_report_safely(run_status)
    end
  end

  def _run_report_safely(run_status)
    run_report_unsafe(run_status)
  rescue Exception => e
    Chef::Log.error("Report handler #{self.class.name} raised #{e.inspect}")
    Array(e.backtrace).each { |line| Chef::Log.error(line) }
  ensure
    @run_status = nil
  end

  def report
    if run_status.failed?
      if run_status.run_context.reboot_requested?
        Chef::Log.warn('Reboot requested, exiting gracefully.')
        run_status.events.handlers_completed
        @run_status = nil
        exit 0
      else
        work_note = "Error: #{run_status.formatted_exception}\n\n"
        work_note << Array(backtrace).join("\n")
        default_payload = {
          'short_description' => "Chef run failed on #{node.name}",
          'work_notes' => work_note,
        }
        payload = default_payload.merge(@incident_payload)
        create_incident(payload)
      end
    else
      Chef::Log.info("Run succeeded on #{node.name}.")
    end
  end
end

require 'servicenow/client'
