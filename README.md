# chef-handler-servicenow

[![Gem Version](https://badge.fury.io/rb/chef-handler-servicenow.svg)](https://badge.fury.io/rb/chef-handler-servicenow)
[![pipeline status](https://gitlab.com/dg42/chef-handler-servicenow/badges/master/pipeline.svg)](https://gitlab.com/dg42/chef-handler-servicenow/commits/master)

A Chef handler to interact with ServiceNow.

## Installation

`gem install chef-handler-servicenow`

## Use

```ruby
# Chef Event handler example
require 'servicenow'
Chef.event_handler do
  on :run_failed do
    client = ServiceNow.new({'instance' => 'dev12345', 'username' => 'user.name', 'password' => 'pa$$word'})
    response = client.create_incident({'short_description' => 'chef-client failed on node-xyz'})
  end
end

# Chef Report/Exception handler example
chef_gem 'httparty'
handler_version = '0.3.0'
chef_gem 'chef-handler-servicenow' do
  version handler_version
  action :install
end
chef_handler 'ServiceNow' do
  source "/opt/chef/embedded/lib/ruby/gems/2.5.0/gems/chef-handler-servicenow-#{handler_version}/lib/servicenow.rb"
  arguments(
    'instance' => 'dev12345',
    'username' => 'user.name',
    'password' => 'pa$$word',
  )
  supports start: false, report: true, exception: true
  action :enable
end
```

## Test

`rake test`